const getSum = (str1, str2) => {
    if (typeof str1 !== 'string' || isNaN(str1) || typeof str2 !== 'string' || isNaN(str2)) {
        return false;
    } else {
        let str = str1 + ' ' + str2;
        return str.match(/-?\d+/g).reduce((sum, el) => sum + +el, 0).toString();
    }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    const authorPosts = listOfPosts.filter((el) => {
        return el.author === authorName;
    });
    const authorComments = [];
    listOfPosts.forEach((el) => {
        if ('comments' in el) {
            authorComments.push(...el.comments.filter((el) => {
                return el.author === authorName;
            }));
        }
    });
    return `Post:${authorPosts.length},comments:${authorComments.length}`;
};

const tickets = (people) => {
    let cash = 0;
    for (let i in people) {
        if (people[0] === 25 && cash < 25) {
            cash = people[0];
        } else if (people[0] > 25 || people[i] - 25 > cash) {
            return 'NO';
        } else {
            cash += 25;
        }
    }
    return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
